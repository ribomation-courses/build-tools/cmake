#!/usr/bin/env bash
set -eux

rm -rf dist tmp
mkdir -p dist tmp

g++-8 -std=c++17 -Wall -c src/util.cxx -o tmp/util.o
g++-8 -std=c++17 -Wall -c src/account.cxx -o tmp/account.o
ar crs dist/accounts-1.42.a tmp/util.o tmp/account.o
cp src/account.hxx dist
rm -rf tmp
tree

