Course in CMake, 2 days
====

Welcome to this course. The syllabus can be found at
[build/cmake](https://www.ribomation.se/courses/build/cmake.html)

Here you will find
* Installation instructions
* Start code for the exercises
* Solutions to the exercises
* Sources to the demo programs

Installation Instructions
====

Installation of Ubuntu Linux
----

We will primarily do the exercises on Linux. One exercise task uses MS Windows.
Therefore you need access to a Linux or Unix system, preferably Ubuntu.

### Windows 10

If you are using Windows 10; install Ubuntu 18.04 @ WSL. Read how to do it here:
[Windows 10: WSL Installation Guide](https://docs.microsoft.com/en-us/windows/wsl/install-win10)

### Earlier version of Windows

If you are using an earlier version of Windows; install VirtualBox, create a
virtual machine and install the latest version of Ubuntu Desktop to the VM.
1. Install VirtualBox (VBox)<br/>
    <https://www.virtualbox.org/wiki/Downloads>
1. Create a new virtual machine (VM) i VBox, for Ubuntu Linux<br/>
    <https://www.virtualbox.org/manual/ch03.html>
1. Download an ISO file for the latest version of Ubuntu Desktop<br/>
    <http://www.ubuntu.com/download/desktop>
1. Mount the ISO file in the virtual CD drive of your VM
1. Start the VM and run the Ubuntu installation program.
    Ensure you install to the (virtual) hard-drive.
    Set a username and password when asked to and write them down.
1. Install the VBox guest additions<br/>
    <https://www.virtualbox.org/manual/ch04.html>

Installation within *NIX
----
Install the latest version of CMake, by downloading the `*.tar.gz` file, unpack it and follow the instructions in Readme.txt.
* https://cmake.org/download/

In addition; you need to install (_the commands below are for Ubuntu_)
* The Make and Ninja builder tools; `sudo apt install make ninja-build`
* The GNU C/C++ compiler; `sudo apt install gcc-8 g++-8`
* The CLang C++ compiler; `sudo apt install clang`
* Git client; `sudo apt install git`
* Any text editor you like

Installation on MS Windows
----
Install the latest version of CMake for Windows, by downloading the `*.msi` file and
run it.
* https://cmake.org/download/

In addition; you need to install
* MS Visual Studio Command-Line Compiler; 
[Tools for Visual Studio 2017 / Build Tools for Visual Studio 2017](https://visualstudio.microsoft.com/downloads/)
* Git client; [GIT Client Download](https://git-scm.com/downloads)


Usage of this GIT Repo
====

Ensure you have a [GIT client](https://git-scm.com/downloads) installed, open a GIT BASH window and clone this repo, by 
following the commands below.

    mkdir -p ~/cmake-course/my-solutions
    cd ~/cmake-course
    git clone https://gitlab.com/ribomation-courses/build-tools/cmake.git gitlab


During the course, solutions will be push:ed to this repo and you can get these by
a git pull operation

    cd ~/cmake-course/gitlab
    git pull


Interesting Videos
====

* [CppCon 2016: Jason Turner “Rich Code for Tiny Computers: A Simple Commodore 64 Game in C++17”](https://youtu.be/zBkNBP00wJE)
* [Hello World from Scratch - Peter Bindels & Simon Brand [ACCU 2019]](https://youtu.be/MZo7k_IOCe8)
* [CppCon 2018: Michael Caisse “Modern C++ in Embedded Systems - The Saga Continues”](https://youtu.be/LfRLQ7IChtg)


***
*If you have any questions, don't hesitate to contact me*<br>
**Jens Riboe**<br/>
Ribomation AB<br/>
[jens.riboe@ribomation.se](mailto:jens.riboe@ribomation.se)<br/>
[www.ribomation.se](https://www.ribomation.se)<br/>
