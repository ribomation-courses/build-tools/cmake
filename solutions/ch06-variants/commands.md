# Setup

    mkdir bld &&  cd bld

# Cleanup
Inside the ./bld/* directory, type

    rm -rf *

# Default

    cmake ..
    cmake --build .
    cmake --build . --target run

# Build Type: Release

    rm -rf *
    cmake -DCMAKE_BUILD_TYPE=Release ..
    cmake --build . -- VERBOSE=1
    cmake --build . --target run

# Build Tool: Ninja

    rm -rf *
    cmake -G Ninja ..
    cmake --build . -- -v
    cmake --build . --target run

# Compiler: CLang C/C++

    rm -rf *
    cmake -DCMAKE_CXX_COMPILER=clang++ -DCMAKE_C_COMPILER=clang ..
    cmake --build . -- VERBOSE=1
    cmake --build . --target run

